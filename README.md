# Simple maven project that generate site and javadoc and publish the result to a git lab page #

State of the project: [![build status](https://gitlab.com/avdyk/gitlab-page-mvn-generation-example/badges/master/build.svg)](https://gitlab.com/avdyk/gitlab-page-mvn-generation-example/commits/master)

```plantuml
Bob -> Alice : hello
Alice -> Bob : Go Away
```

## Generated website by maven

https://avdyk.gitlab.io/gitlab-page-mvn-generation-example/

## Project UI

```plantuml
@startsalt
{
  Login    | "MyName   "
  Password | "****     "
  [Cancel] | [  OK   ]
}
@endsalt
```

See documentation to design UI with PlantUML: https://plantuml.com/fr/salt

## Gantt

```plantuml
@startgantt
[Test du prototype] lasts 10 days
[Prototype terminé] happens at [Test du prototype]'s end
[Mise en place production] lasts 12 days
[Mise en place production] starts at [Test du prototype]'s end
@endgantt
```

See documentation: https://plantuml.com/fr/gantt-diagram

## MindMap

```plantuml
@startmindmap
caption figure 1
title My super title

* <&flag>Debian
** <&globe>Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** <&graph>LMDE
** <&pulse>SolydXK
** <&people>SteamOS
** <&star>Raspbian with a very long name
*** <s>Raspmbc</s> => OSMC
*** <s>Raspyfi</s> => Volumio

header
My super header
endheader

center footer My super footer

legend right
  Short
  legend
endlegend
@endmindmap
```

See documentation: https://plantuml.com/fr/mindmap-diagram

package com.gitlab.avdyk.example.site;

/**
 * The Foo interface.
 * 
 * It contains the bar method.
 *
 */
public interface Foo {

    /**
     * The bar method.
     * 
     * @param arg the argument.
     * @return the argument reversed.
     */
    String bar(final String arg);

}

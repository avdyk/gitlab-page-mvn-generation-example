package com.gitlab.avdyk.example.site;

/**
 * A Bar interface with a foo method.
 */
public interface Bar {

    /**
     * Create a new {@link Foo}.
     *
     * @return a new {@link Foo}.
     */
    Foo createFoo();

}
